/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applaorden;

import java.awt.*;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.*;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.WindowConstants;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author AMAVILANH
 */



class MiModelo extends DefaultTableModel
{
   public boolean isCellEditable (int row, int column)
   {
       // Aquí devolvemos true o false según queramos que una celda
       // identificada por fila,columna (row,column), sea o no editable
    /*   if (column == 3)
          return true;  */
       return false;
   }
}

class NodoProducto {

    int id;
    String nombre;
    int precio;
    int[] contenido;
    JButton boton;
    NodoProducto siguiente;

    public NodoProducto(int n, String nomb, int p, int[] vec) {
        this.id = n;
        this.nombre = nomb;
        this.precio = p;
        this.boton = new JButton(nomb);
        int aux = vec.length;
        this.contenido = new int[aux];
        for (int i = 0; i < aux; i++) {
            this.contenido[i] = vec[i];
        }
        this.siguiente = null;
    }
}

class ListaProducto{

    protected NodoProducto inicio, fin; //Punteros para saber donde esta el inicio y el fin
    public ListaProducto() {    //Constructor para inicializar punteros
        inicio = null;
        fin = null;
    }

    //Metodo para saber si la lista esta vacia
    public boolean estaVacia() {
        return inicio == null;
    }

    
    //Metodo para insertar a la lista un objeto Nodo
    public void agregaralfinal(NodoProducto nodaso) {
        if (!estaVacia()) {
            fin.siguiente = new NodoProducto(nodaso.id, nodaso.nombre, nodaso.precio, nodaso.contenido);
            fin = fin.siguiente;
        } else {
            inicio = fin = new NodoProducto(nodaso.id, nodaso.nombre, nodaso.precio, nodaso.contenido);
        }
    }    
    
    //Metodo para insertar al final de la lista
    public void agregaralfinal(int n, String nomb, int p, int[] vec) {
        if (!estaVacia()) {
            fin.siguiente = new NodoProducto(n, nomb, p, vec);
            fin = fin.siguiente;
        } else {
            inicio = fin = new NodoProducto(n, nomb, p, vec);
        }
    }

    //Metodo para devolver un elemento por indice
    public NodoProducto elemento(int indice){
        NodoProducto recorrer = inicio;
        while(recorrer!=null){
            if((indice+1)==recorrer.id){
                break;
            }
            else{
            recorrer=recorrer.siguiente;
            }
        }
        return recorrer;
    }
    
    
    public void eliminar(int elemento){
        elemento++;
        if(!estaVacia()){
            if(inicio==fin && elemento==inicio.id){
                inicio=null;
                fin=null;
            }else if(elemento==inicio.id){
                inicio=inicio.siguiente;
            }else{
                NodoProducto anterior,temporal;
                anterior=inicio;
                temporal=inicio.siguiente;
                while(temporal!=null && temporal.id!=elemento){
                    anterior=anterior.siguiente;
                    temporal=temporal.siguiente;
                }
                if(temporal!=null){
                    anterior.siguiente=temporal.siguiente;
                    if(temporal==fin){
                        fin=anterior;
                    }
                }
            }
        }
    }    
    
}


public class APPLAORDEN implements ActionListener, ListSelectionListener  {

    /**
     * @param args the command line arguments
     */
    JFrame ventana, Producp;
    private JMenuBar barramenu;
    private JMenu menuarchivo;
    private JPanel PPrincipal, PanelP;
    private JList lista, est, inv, listaproducto,listaseleccion;
    private JTable tablaseleccion;
    private DefaultListModel modelo, modeloproducto, modeloseleccion;
    private DefaultTableModel tablemodel;
    private MiModelo tableselec;
    private JScrollPane scrollcuenta, invscroll, estscroll, scrolltable, scrollproducto, scrollseleccion;
    private JButton bt1, bt2, bt3, bt4, bt5, aceptar, eliminar, terminar;
    private ArrayList<String> nombreArrayList = new ArrayList<String>();
    private ArrayList<int []> Seleccion;
    private ArrayList<ArrayList> listacuentas;
    private ListaProducto Listita;
    private ListSelectionModel listselectionmod;
    private int select;
    private int[][] precios;

    public void imprimeproductos() throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(("productos.txt"));
        BufferedReader b = new BufferedReader(f);
        while ((cadena = b.readLine()) != null) {
            System.out.println(cadena);
        }
        b.close();
    }

    public void construyeventana() {
        ventana = new JFrame("MANGOROCKER");
        //ventana.setSize(1000, 500);
        //ventana.setExtendedState(MAXIMIZED_BOTH);
        ventana.getContentPane().setLayout(new GridBagLayout());
        barramenu = new JMenuBar();
        JMenu menuarchivo = new JMenu("Archivo");
        JMenu menucuentas = new JMenu("Cuentas");
        JMenu menuinventario = new JMenu("Inventario");
        JMenu menusalidas = new JMenu("Salidas");
        barramenu.add(menuarchivo);
        barramenu.add(menucuentas);
        barramenu.add(menuinventario);
        barramenu.add(menusalidas);
        ventana.setJMenuBar(barramenu);

        JLabel et1 = new JLabel("Cuentas");
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 7;
        constraints.gridheight = 1;
        constraints.weightx = 0.33;
        constraints.insets = new Insets(4, 1, 0, 0);
        constraints.anchor = (GridBagConstraints.NORTH);
        ventana.getContentPane().add(et1, constraints);

        //String [] nombre = {"Pink Floyd","Beatles","Nirvana","Iggy Pop,Pink Floyd","Beatles","Nirvana","Iggy Pop,Pink Floyd","Beatles","Nirvana","Iggy Pop"};
        lista = new JList();
        lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        modelo = new DefaultListModel();
        listacuentas=new ArrayList<ArrayList>();
        listselectionmod= lista.getSelectionModel();
        listselectionmod.addListSelectionListener(this);
        lista.setSelectionModel(listselectionmod);
        //Object [] seleccion = lista.getSelectedValues();
        //int [] indices = lista.getSelectedIndices();
        scrollcuenta = new JScrollPane(lista);
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 7;
        constraints.gridheight = 11;
        constraints.weightx = 0.33;
        constraints.weighty = 1;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = new Insets(0, 1, 2, 3);
        ventana.getContentPane().add(scrollcuenta, constraints);

        //JLabel et2 = new JLabel("Panel lista en cuenta");
        String[] cnames = {"Cant", "Producto", "Valor c/u", "Valor Total"};
        String[][] data = {{"1", "Producto", "valor", "total"}, {"2", "Producto", "valor", "total"}, {"3", "Producto", "valor", "total"}};
        JTable table = new JTable(data, cnames);
        tablemodel = new DefaultTableModel();
        tablemodel.addColumn("Cant");
        tablemodel.addColumn("Producto");
        tablemodel.addColumn("Valor c/u");
        tablemodel.addColumn("Valor Total");
        constraints.gridx = 11;
        constraints.gridy = 1;
        constraints.gridwidth = 17;
        constraints.gridheight = 12;
        constraints.weightx = 1;
        constraints.weighty = 1;
        constraints.insets = new Insets(4, 3, 1, 2);
        scrolltable = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        ventana.getContentPane().add(scrolltable, constraints);

        JLabel et3 = new JLabel("Estadisticas");
        constraints.gridx = 20;
        constraints.gridy = 14;
        constraints.gridwidth = 8;
        constraints.gridheight = 1;
        constraints.weighty = 0;
        constraints.insets = new Insets(1, 1, 0, 2);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        ventana.getContentPane().add(et3, constraints);

        String[] estnom = {"Mesas", "Clientes", "Plata", "Locura", "Porros", "Fumas", "Mesas", "Clientes", "Plata", "Locura", "Porros", "Fumas"};
        est = new JList(estnom);
        est.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        Object[] estselec = est.getSelectedValues();
        int[] estind = est.getSelectedIndices();
        estscroll = new JScrollPane(est);
        constraints.gridx = 20;
        constraints.gridy = 15;
        constraints.gridwidth = 8;
        constraints.gridheight = 5;
        constraints.insets = new Insets(0, 1, 2, 2);
        ventana.getContentPane().add(estscroll, constraints);

        JLabel et4 = new JLabel("Inventario");
        constraints.gridx = 11;
        constraints.gridy = 14;
        constraints.gridwidth = 8;
        constraints.gridheight = 1;
        constraints.insets = new Insets(1, 3, 0, 0);
        ventana.getContentPane().add(et4, constraints);

        String[] invnom = {"Poker", "Aguila", "Club D", "Club R", "Club N", "Light", "Reeds", "Budweiser", "Poker", "Aguila", "Club D", "Club R", "Club N", "Light", "Reeds", "Budweiser"};
        inv = new JList(invnom);
        inv.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        Object[] invselec = inv.getSelectedValues();
        int[] invind = inv.getSelectedIndices();
        invscroll = new JScrollPane(inv);
        constraints.gridx = 11;
        constraints.gridy = 15;
        constraints.gridwidth = 8;
        constraints.gridheight = 5;
        constraints.insets = new Insets(0, 3, 1, 2);
        ventana.getContentPane().add(invscroll, constraints);

        bt1 = new JButton("Agregar Cuenta");
        bt1.addActionListener(this);
        constraints.weightx = 0;
        constraints.gridx = 1;
        constraints.gridy = 15;
        constraints.gridwidth = 7;
        constraints.gridheight = 1;
        constraints.insets = new Insets(2, 1, 0, 3);
        ventana.getContentPane().add(bt1, constraints);

        bt2 = new JButton("Agregar Producto");
        bt2.addActionListener(this);
        constraints.gridx = 1;
        constraints.gridy = 16;
        constraints.gridwidth = 7;
        constraints.gridheight = 1;
        constraints.insets = new Insets(0, 1, 0, 3);
        ventana.getContentPane().add(bt2, constraints);

        bt3 = new JButton("Separar Cuenta");
        constraints.gridx = 1;
        constraints.gridy = 17;
        constraints.gridwidth = 7;
        constraints.gridheight = 1;
        ventana.getContentPane().add(bt3, constraints);

        bt4 = new JButton("Pago Parcial");
        constraints.gridx = 1;
        constraints.gridy = 18;
        constraints.gridwidth = 7;
        constraints.gridheight = 1;
        ventana.getContentPane().add(bt4, constraints);

        bt5 = new JButton("Pago Total");
        constraints.gridx = 1;
        constraints.gridy = 19;
        constraints.gridwidth = 7;
        constraints.gridheight = 1;
        constraints.insets = new Insets(0, 1, 1, 3);
        ventana.getContentPane().add(bt5, constraints);

        ventana.pack();

        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        ventana.setVisible(true);
    }

    public void leedb() throws FileNotFoundException{
        Listita  = new ListaProducto();
        modeloproducto= new DefaultListModel();
        String cadena;
        int numero, ide, pr, tr, aux;
        int[] conte;
        FileReader f = new FileReader(("productos.txt"));
        Scanner scan = new Scanner(f);
        numero=scan.nextInt();
        precios=new int[numero][2];
        tr=scan.nextInt();
        conte = new int[tr];
        for(int i=0;i<numero;i++){
            ide=scan.nextInt();
            scan.nextLine();
            cadena=scan.nextLine();
            pr=scan.nextInt();
            for(int j=0;j<tr;j++){
                aux=scan.nextInt();
                conte[j]=aux;
            }
            precios[i][0]=ide;
            precios[i][1]=pr;
            modeloproducto.addElement(cadena);
            Listita.agregaralfinal(ide, cadena, pr, conte);
            
        }
        
        scan.close();

    }

    
    private void agregarcuenta() {
        String nombrec = JOptionPane.showInputDialog(null, "Escribe el nombre de la cuenta a agregar", "Agregar Cuenta", JOptionPane.QUESTION_MESSAGE);
        modelo.addElement(nombrec);
        lista.setModel(modelo);

    }

    private void agregarproducto(){
        Producp = new JFrame("Productos");
        Producp.setLayout(new BoxLayout(Producp.getContentPane(), BoxLayout.Y_AXIS));
        JPanel panelSuperior, panelInferior, panelizq, panelder,panelter;
        JLabel pantalla;
        panelSuperior = new JPanel();
        pantalla = new JLabel("Bienvenidos a Heavy Machine Gun");
        panelSuperior.add(pantalla);
        panelInferior = new JPanel();
        panelInferior.setLayout(new FlowLayout());
        panelder= new JPanel();
        panelder.setLayout(new BoxLayout(panelder, BoxLayout.Y_AXIS));
        panelizq= new JPanel();
        panelizq.setLayout(new BoxLayout(panelizq, BoxLayout.Y_AXIS));
        listaproducto = new JList();
        listaproducto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listaseleccion = new JList();
        listaseleccion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tablaseleccion = new JTable();
        tableselec = new MiModelo();
        tableselec.addColumn("#");
        tableselec.addColumn("Producto");
        Seleccion = new ArrayList();
        select=0;
        modeloseleccion= new DefaultListModel();
        listaproducto.setModel(modeloproducto);
        listaseleccion.setModel(modeloseleccion);
        tablaseleccion.setModel(tableselec);
        //tablaseleccion.setPreferredScrollableViewportSize(listaproducto.getSize());
        tablaseleccion.setPreferredScrollableViewportSize(listaproducto.getPreferredScrollableViewportSize());
        scrollproducto = new JScrollPane(listaproducto);
        scrollseleccion = new JScrollPane(tablaseleccion);
        aceptar = new JButton("Agregar");
        aceptar.addActionListener(this);
        eliminar = new JButton("Eliminar");
        eliminar.addActionListener(this);
        panelter = new JPanel();
        terminar= new JButton("Terminar");
        terminar.addActionListener(this);
        panelter.add(terminar);
        panelizq.add(scrollproducto);
        panelizq.add(aceptar);
        panelder.add(scrollseleccion);
        panelder.add(eliminar);
        panelInferior.add(panelizq);
        panelInferior.add(panelder);
        Producp.add(panelSuperior);        
        Producp.add(panelInferior);
        Producp.add(panelter);
        Producp.pack();
        Producp.setVisible(true);
        Producp.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    }
    
            public void agregar(int indice){
                if (indice>=0) {
                    int cantidad=0;
                    try{
                        cantidad=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa la cantidad", "CANTIDAD", JOptionPane.QUESTION_MESSAGE));
                    }
                    catch(NumberFormatException n){
                        JOptionPane.showMessageDialog(null, "Error" + n.getMessage());
                    }
                    if(cantidad>0){
                        int[] selec = new int[2];
                        String [] aux = new String[2];
                        String[] nombpro=new String[4];
                        nombpro[0]= 
                        nombpro[1]=modeloproducto.getElementAt(indice).toString();
                        nombpro[2]=modeloproducto.getElementAt(indice).toString();
                        tableselec.addRow(nombpro);
                        modeloseleccion.addElement(modeloproducto.getElementAt(indice));
                        select++;
                        selec[0]=indice;
                        selec[1]=cantidad;
                        Seleccion.add(selec);
                        listaseleccion.setModel(modeloseleccion);
                        tablaseleccion.setModel(tableselec);
                        }
                    else{
                        JOptionPane.showMessageDialog(null, "Debe ser un numero mayor a 0","Error", JOptionPane.ERROR_MESSAGE);
                    }
                    }else{
                       JOptionPane.showMessageDialog(null, "Debe seleccionar un indice"
                       ,"Error", JOptionPane.ERROR_MESSAGE);
             }
            }

            public void eliminar(int indice){
                if (indice>=0) {
                    modeloseleccion.removeElementAt(indice);
                    tableselec.removeRow(indice);
                    tablaseleccion.setModel(tableselec);
                    listaseleccion.setModel(modeloseleccion);
                    Seleccion.remove(indice);
                    }else{
                       JOptionPane.showMessageDialog(null, "Debe seleccionar un indice"
                       ,"Error", JOptionPane.ERROR_MESSAGE);
             }
            }
            
            public void terminar(){
                
                listacuentas.add(Seleccion);
                Producp.setVisible(false);
                
            }
    
    private void separarcuenta() {

    }

    private void pagoparcial() {

    }

    private void pagototal() {

    }

 
    public APPLAORDEN() throws FileNotFoundException {
        
        leedb();
        construyeventana();
    }

    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic 
        new APPLAORDEN();
    }  


    public void actionPerformed(ActionEvent evento) {
        if (evento.getSource() == bt1) {
            agregarcuenta();
        }
        if (evento.getSource() == bt2) {
            agregarproducto();

        }
        if (evento.getSource()== aceptar) {
           agregar(listaproducto.getSelectedIndex());
        }
        if (evento.getSource()== eliminar) {
           eliminar(listaseleccion.getSelectedIndex());
        }
        if (evento.getSource()== terminar) {
           terminar();
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) { 
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            int firstIndex = e.getFirstIndex();
            int lastIndex = e.getLastIndex();
            boolean isAdjusting = e.getValueIsAdjusting(); 
           
            if (lsm.isSelectionEmpty()) {
                //algo no pasara
            } else {
                if(isAdjusting){
                int minIndex = lsm.getMinSelectionIndex();
                int maxIndex = lsm.getMaxSelectionIndex();
                System.out.println(minIndex);
                }
                
            }
        }

}





    /*   public void cuentas(){ 
        JLabel cuenta = new JLabel("Cuentas"); 
        String [] nombre = {"Pink Floyd","Beatles","Nirvana","Iggy Pop"};
        lista = new JList(nombre);
        lista.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        Object [] seleccion = lista.getSelectedValues();
        int [] indices = lista.getSelectedIndices();
        JScrollPane barradesplazamiento = new JScrollPane(lista);
    }   */
 /*    public void principal(){
    PPrincipal = new JPanel();
    String [] nombre = {"Pink Floyd","Beatles","Nirvana","Iggy Pop"};
    JList lista = new JList(nombre);
    lista.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    Object [] seleccion = lista.getSelectedValues();
    int [] indices = lista.getSelectedIndices();
    JScrollPane barradesplazamiento = new JScrollPane(lista);
    PPrincipal.add(barradesplazamiento);
    } 
     */



            //JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana", "Mensaje en la barra de titulo", JOptionPane.WARNING_MESSAGE);
            //JOptionPane.showMessageDialog(null,"La cuenta ha sido exitosamente agregada", "Operacion Exitosa", JOptionPane.INFORMATION_MESSAGE);

            //JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana", "Mensaje en la barra de titulo", JOptionPane.WARNING_MESSAGE);
            //JOptionPane.showMessageDialog(null,"La cuenta ha sido exitosamente agregada", "Operacion Exitosa", JOptionPane.INFORMATION_MESSAGE);





